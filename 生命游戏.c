#include <stdio.h>  //函数外全局变量定义
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

#define High 20 //定义游戏画面尺寸
#define Width 30

//全局变量
int cells[High][Width];


void gotoxy(int x,int y)    //光标移动到（x,y)位置
{
    HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X=x;
    pos.Y=y;
    SetConsoleCursorPosition(handle,pos);
}

void startup()//数据初始化
{
    int i,j;
    for (i=0;i<High;i++)
        for (j=0;j<Width;j++)
        cells[i][j] = 1;
}
   void  show()  //显示画面
   {
       gotoxy(0,0);//光标移动到原点位置，以下重画清屏
      int i,j;

      for (i=0;i<High;i++)
      {
          for (j=0;j<Width;j++)
          {
             if (cells[i][j] == 1)
                printf("*");
            else
                printf(" ");
          }
          printf ("\n");
      }
   }
   void  updateWithoutInput() //与用户输入无关的更新
   {
       int i,j;
       int tempCells[High][Width];  //临时存储的二维数组
       for (i=0;i<High;i++)
          for (j=0;j<Width;j++)
            tempCells[i][j] = cells[i][j];

       int NeibourNumber = 0;
       for (i=1;i<High-1;i++)
       {
           for(j=1;j<Width-1;j++)
           {
             NeibourNumber = cells[i-1][j-1] + cells[i-1][j]+ cells[i-1][j+1]
                           + cells[i][j-1] + cells[i][j]+ cells[i][j+1]
                           + cells[i+1][j-1] + cells[i+1][j]+ cells[i+1][j+1];
            if(NeibourNumber == 3)
               tempCells[i][j] = 1;
            else if (NeibourNumber == 2)
               tempCells[i][j] = cells[i][j];
            else
                tempCells[i][j] = 0;
           }
       }
       for (i=0;i<High;i++)
          for (j=0;j<Width;j++)
            cells[i][j] = tempCells[i][j];

       Sleep(150);
   }
   void  updateWithInput()  //与用户输入有关的更新
   {

   }
int main ()
{
    startup ();   //数据初始化
    while(1)//游戏循环执行
    {
     show();  //显示画面
     updateWithoutInput();  //与用户输入无关的更新
     updateWithInput();  //与用户输入有关的更新
    }
    return 0;
}
