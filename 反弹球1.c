#include <stdio.h>  //函数外全局变量定义
#include <stdlib.h>
#include <conio.h>
#include <windows.h>


//全局变量



void gotoxy(int x,int y)    //光标移动到（x,y)位置
{
    HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X=x;
    pos.Y=y;
    SetConsoleCursorPosition(handle,pos);
}

void startup()//数据初始化
{

}
   void  show()  //显示画面
   {
       gotoxy(0,0);//光标移动到原点位置，以下重画清屏



        printf ("\n");

   }
   void  updateWithoutInput() //与用户输入无关的更新
   {


       Sleep(150);
   }

   void  updateWithInput()  //与用户输入有关的更新
   {

   }
int main ()
{
    startup ();   //数据初始化
    while(1)//游戏循环执行
    {
     show();  //显示画面
     updateWithoutInput();  //与用户输入无关的更新
     updateWithInput();  //与用户输入有关的更新
    }
    return 0;
}
