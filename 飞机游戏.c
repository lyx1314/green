#include <stdio.h>  //函数外全局变量定义
#include <stdlib.h>
#include <conio.h>
#include <windows.h>
int position_x,position_y;//飞机位置
int bullet_x,bullet_y;   //子弹位置
int high,width;   //游戏画面尺寸
int enemy_x,enemy_y;   //敌机位置
int score;   //游戏得分

void gotoxy(int x,int y)    //光标移动到（x,y)位置
{
    HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X=x;
    pos.Y=y;
    SetConsoleCursorPosition(handle,pos);
}
void HideCursor()
{
    CONSOLE_CURSOR_INFO cursor_info = {1,0};//第二个值为0表示隐藏光标
    SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
}

void startup()//数据初始化
{
    high = 18;
    width = 30;

    position_x = high/2;
    position_y = width/2;

    bullet_x = 0;
    bullet_y = position_y;

    enemy_x = 0;
    enemy_y = width/2;

    score = 0;
    HideCursor();//隐藏光标
}
   void  show()  //显示画面
   {
       gotoxy(0,0);//光标移动到原点位置，以下重画清屏
      int i,j;

      for (i=0;i<high;i++)
      {
          for (j=0;j<width;j++)
          {
              if ((i==position_x)&&(j==position_y))
                printf("*");   //输出飞机*
                else if ((i==bullet_x)&&(j==bullet_y))
                printf("|");   //输出子弹|
                else if ((i==enemy_x)&&(j==enemy_y))
                printf("@");   //输出敌机@
                else
                    printf(" ");//输出空格
          }
          printf ("\n");
      }
      printf ("得分：%d",score);
   }
   void  updateWithoutInput() //与用户输入无关的更新
   {
        if ((bullet_x==enemy_x)&&(bullet_y==enemy_y))
        {
            score++;
            enemy_x=0;
             enemy_y = rand()% width;
            bullet_x = -1;
        }

      static int speed = 0;
       if (speed<10)
        speed++;
       if (bullet_x>-1)
       bullet_x--;

       if (enemy_x>high)
       {
        enemy_x = 0;
        enemy_y = rand()% width;
       }
       else
       {
           if(speed==10)
           {
             enemy_x++;
             speed = 0;
           }
      }
   }
   void  updateWithInput()  //与用户输入有关的更新
   {

     char input;

     if (kbhit())   //当按键时执行
     {
         input = getch();

         if (input=='a')
            position_y--;
         if (input=='d')
            position_y++;
         if (input=='w')
            position_x--;
         if (input=='s')
            position_x++;
         if (input==' ')
         {
           bullet_x = position_x-1;
           bullet_y = position_y;
         }

     }
   }
int main ()
{
    startup ();   //数据初始化
    while(1)//游戏循环执行
    {
     show();  //显示画面
     updateWithoutInput();  //与用户输入无关的更新
     updateWithInput();  //与用户输入有关的更新
    }
    return 0;
}
